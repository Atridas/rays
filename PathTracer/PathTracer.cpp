// RayTracer.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include <cinttypes>
#include <vector>
#include <cassert>
#include <random>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include"stb_image_write.h"

constexpr double tau = 6.283185307179586476925286766559;
constexpr double pi = tau*0.5;

struct AntiVector;
struct Vector
{
	double x, y, z;

	double square() const
	{
		return x * x + y * y + z * z;
	}

	bool unit() const
	{
		return fabs(square() - 1) < 0.0001;
	}

	Vector operator+(Vector o) const
	{
		return { x + o.x, y + o.y, z + o.z };
	}

	Vector operator-(Vector o) const
	{
		return { x - o.x, y - o.y, z - o.z };
	}

	Vector operator*(double s) const
	{
		return { s * x, s * y, s * z };
	}

	Vector operator/(double s) const
	{
		return { x / s, y / s, z / s };
	}

	Vector normalized() const
	{
		assert(square() > 0);
		return *this / sqrt(square());
	}

	AntiVector rightComplement() const;
	AntiVector leftComplement() const;
};

inline Vector operator*(double s, Vector v)
{
	return { s * v.x, s * v.y, s * v.z };
}

struct AntiVector
{
	double nx, ny, nz;

	Vector rightComplement() const
	{
		return { nx, ny, nz };
	}

	Vector leftComplement() const
	{
		return { -nx, -ny, -nz };
	}

	bool operator==(AntiVector other) const
	{
		return nx == other.nx && ny == other.ny && nz == other.nz;
	}

	AntiVector operator+(AntiVector other) const
	{
		return { nx + other.nx, ny + other.ny, nz + other.nz };
	}

	AntiVector operator-(AntiVector other) const
	{
		return { nx - other.nx, ny - other.ny, nz - other.nz };
	}

	AntiVector operator-() const
	{
		return { -nx, -ny, -nz };
	}

	AntiVector operator*(double s) const
	{
		return { s * nx, s * ny, s * nz };
	}

	double square() const
	{
		return nx * nx + ny * ny + nz * nz;
	}

	bool unit() const
	{
		return fabs(square() - 1) < 0.0001;
	}

	AntiVector normalized() const
	{
		assert(square() > 0);
		return *this * (1.0 / sqrt(square()));
	}
};


AntiVector operator*(double s, AntiVector v)
{
	return { s * v.nx, s * v.ny, s * v.nz };
}

inline AntiVector OuterProduct(Vector a, Vector b)
{
	return { a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x };
}

inline double OuterProduct(AntiVector a, Vector b)
{
	return a.nx * b.x + a.ny * b.y + a.nz * b.z;
}

AntiVector Vector::rightComplement() const
{
	return { x, y, z };
}

AntiVector Vector::leftComplement() const
{
	return { -x, -y, -z };
}

double InnerProduct(Vector a, Vector b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

double InnerProduct(AntiVector a, AntiVector b)
{
	return -a.nx * b.nx - a.ny * b.ny - a.nz * b.nz;
}

inline Vector InnerRightProduct(AntiVector a, Vector b)
{
	return { a.nz*b.y - a.ny*b.z, a.nx*b.z - a.nz*b.x, a.ny*b.x - a.nx*b.y };
}

inline Vector AntiwedgeProduct(AntiVector a, AntiVector b)
{
	return OuterProduct(a.rightComplement(), b.rightComplement()).leftComplement();
}

struct Ray
{
	Vector origin;
	AntiVector dir;
};

struct Sphere
{
	Vector center;
	double r;
};

inline double Intersection(Ray ray, Sphere sphere)
{
	assert(ray.dir.unit());

	Vector c = sphere.center - ray.origin;
	double dc = OuterProduct(ray.dir, c);
	double discriminant = dc * dc - c.square() + sphere.r * sphere.r;
	if (discriminant < 0)
		return -1;

	double u = dc - sqrt(discriminant);

	return u > 0 ? u : dc + sqrt(discriminant);
}

struct Color
{
	double r, g, b;

	Color operator*(double s) const
	{
		return { s * r, s * g, s * b };
	}

	Color operator*(Color c) const
	{
		return { r * c.r, g * c.g, b * c.b };
	}

	Color operator+(Color c) const
	{
		return { r + c.r, g + c.g, b + c.b };
	}
};

enum MaterialType
{
	Diffuse, Specular, Refractive
};

struct Material
{
	Color emmission, color;
	MaterialType type;
};

struct SphereObject
{
	Sphere sphere;
	Material material;
	SphereObject *primaryLightSource;
};

struct Scene
{
	SphereObject *spheres;
	int sphereCount;
};

struct IntersectionPoint
{
	Vector p;
	AntiVector normal;
	Material material;
	SphereObject *primaryLightSource;
};

std::uniform_real_distribution<double> zeroToOne(0, 1);

bool intersect(IntersectionPoint &intersectionPoint_, const Scene& scene, Ray ray) {
	double inf = 1e20, t = 1e20;
	SphereObject *sphereObject = scene.spheres;

	for (int i = 0; i < scene.sphereCount; ++i)
	{
		double t2 = Intersection(ray, scene.spheres[i].sphere);
		if (t2 > 0.0001 && t2 < t)
		{
			t = t2;
			sphereObject = &scene.spheres[i];
		}
	}

	Vector p = ray.origin + ray.dir.rightComplement() * t;
	intersectionPoint_.p = p;
	intersectionPoint_.material = sphereObject->material;
	Vector normal = (p - sphereObject->sphere.center).normalized();
	intersectionPoint_.normal.nx = normal.x;
	intersectionPoint_.normal.ny = normal.y;
	intersectionPoint_.normal.nz = normal.z;
	intersectionPoint_.primaryLightSource = sphereObject->primaryLightSource;

	return t<inf;
}

Color Radiance(const Scene& scene, const Ray& ray, int depth, std::mt19937 &rnd) {

	IntersectionPoint intersectionPoint;
	if (depth < 20 && intersect(intersectionPoint, scene, ray))
	{
		if (intersectionPoint.primaryLightSource == nullptr) // cut the ray at the light sources. This introduces bias (I guess)
			return intersectionPoint.material.emmission;

		AntiVector orientedNormal = InnerProduct(intersectionPoint.normal, ray.dir) > 0 ? intersectionPoint.normal : -intersectionPoint.normal;
		Color color = intersectionPoint.material.color;

		double endThreshold = color.r > color.g ? (color.r > color.b ? color.r : color.b) : (color.b > color.g ? color.b : color.g);
		if (depth > 6)
		{
			if (zeroToOne(rnd) < endThreshold)
				color = color * (1 / endThreshold);
			else
				return intersectionPoint.material.emmission;
		}

		AntiVector dir;
		Color shadowRayRadiance = {};
		double radianceScale = 1.0;
		switch (intersectionPoint.material.type)
		{
		case MaterialType::Diffuse:
		{
			std::uniform_real_distribution<double> angleDist(0, tau);

			double angle = angleDist(rnd);
			double r = zeroToOne(rnd);
			double distance = sqrt(r);
			double invDistance = sqrt(1 - r);

			AntiVector w = orientedNormal;
			AntiVector u = AntiwedgeProduct(w, fabs(w.nx) > 0.1 ? AntiVector{ 0, 1, 0 } : AntiVector{ 1, 0, 0 }).rightComplement().normalized();
			AntiVector v = AntiwedgeProduct(w, u).rightComplement().normalized();

			dir = (u * (cos(angle) * distance) + v * (sin(angle) * distance) + w * invDistance).normalized();

			// shadow ray
			SphereObject &light = *intersectionPoint.primaryLightSource;
			AntiVector shadowRayW = (light.sphere.center - intersectionPoint.p).rightComplement();
			AntiVector shadowRayU = AntiwedgeProduct(shadowRayW, (abs(shadowRayW.nx) > 0.1) ? AntiVector{ 0, 1, 0 } : AntiVector{ 1, 0, 0 }).rightComplement().normalized();
			AntiVector shadowRayV = AntiwedgeProduct(shadowRayW, shadowRayU).rightComplement();

			double maxCosToSun = sqrt(1 - light.sphere.r*light.sphere.r / InnerProduct(light.sphere.center - intersectionPoint.p, light.sphere.center - intersectionPoint.p));

			double e1 = zeroToOne(rnd);
			double e2 = zeroToOne(rnd);

			double cosToSun = 1 - e1 + e1 * maxCosToSun;
			double sinToSun = sqrt(1 - cosToSun*cosToSun);

			double phi = tau * e2;

			AntiVector dirToSunUnn = shadowRayU * cos(phi) * sinToSun + shadowRayV * sin(phi) * sinToSun + shadowRayW * cosToSun;
			AntiVector dirToSun = dirToSunUnn.normalized();

			
			IntersectionPoint shadowIntersectionPoint;
			double cosNormalSun = -InnerProduct(dirToSun, intersectionPoint.normal.normalized());
			if (cosNormalSun > 0 && intersect(shadowIntersectionPoint, scene, { intersectionPoint.p, dirToSun }))
			{
				double rayProbability = tau * (1 - cosToSun);
				shadowRayRadiance = shadowIntersectionPoint.material.emmission * intersectionPoint.material.color * (cosNormalSun / ( rayProbability * pi));
			}
			/*radianceScale = 0.5;*/
            

			break;
		}
		case MaterialType::Specular:
		{
			dir = ray.dir + orientedNormal * 2 * InnerProduct(orientedNormal, ray.dir);
			break;
		}
		case MaterialType::Refractive:
		{
			AntiVector reflection = ray.dir + orientedNormal * 2 * InnerProduct(orientedNormal, ray.dir);
			bool goingIn = (orientedNormal == intersectionPoint.normal);

			double na = goingIn ? 1.0 : 1.6;
			double nb = goingIn ? 1.6 : 1.0;

			Vector d = ray.dir.rightComplement();
			Vector n = orientedNormal.rightComplement();
			Vector tdir = (d * na / nb - n * sqrt(1 - (na*na)*(1 - InnerProduct(d, n)*InnerProduct(d, n)) / (nb*nb)));

			if (tdir.square() > 0)
			{
				tdir = tdir.normalized();
				double perpendicularity = InnerProduct(orientedNormal, ray.dir);
				double R0 = (1.6 - 1.0)*(1.6 - 1.0) / ((1.6 + 1.0)*(1.6 + 1.0));
				double fresnel = R0 + (1 - R0) * pow(1.0 - perpendicularity, 5.0);

				if (zeroToOne(rnd) < fresnel)
					dir = reflection;
				else
					dir = tdir.rightComplement();
			}
			else
			{
				dir = reflection;
			}
			break;
		}
		default:
			assert(false);
			return Color{};
		}

		Color radiance = Radiance(scene, { intersectionPoint.p, dir }, depth + 1, rnd);
		return color * radiance * radianceScale + shadowRayRadiance;
	}
	else
	{
		return Color{};
	}
}

int main()
{
	//int width = 3840, height = 2160;
	//int width = 1920, height = 1080;
	//int width = 1280, height = 720;
	int width = 640, height = 360;

#if 1
	SphereObject spheres[] = {//Scene:                   , emission,        color,      material 
		SphereObject{ Sphere{ {  100001, 40.8, 81.6 } , 100000 },{ {},{ .75,.25,.25 }   , MaterialType::Diffuse } },//Left 
		SphereObject{ Sphere{ {  -99901, 40.8, 81.6 } , 100000 },{ {},{ .25,.25,.75 }   , MaterialType::Diffuse } },//Rght 
		SphereObject{ Sphere{ { 50,40.8, 100000 }     , 100000 },{ {},{ .75,.75,.75 }   , MaterialType::Diffuse } },//Back 
		SphereObject{ Sphere{ { 50,40.8, -99830 }     , 100000 },{ {},{}                , MaterialType::Diffuse } },//Frnt 
		SphereObject{ Sphere{ { 50, 100000  , 81.6 }  , 100000 },{ {},{ .75,.75,.75 }   , MaterialType::Diffuse } },//Botm 
		SphereObject{ Sphere{ { 50, -99918.4, 81.6 }  , 100000 },{ {},{ .75,.75,.75 }   , MaterialType::Diffuse } },//Top 
		SphereObject{ Sphere{ { 27,16.5,47 }          , 16.5   },{ {},{ .999,.999,.999 }, MaterialType::Specular } },//Mirr 
		SphereObject{ Sphere{ { 73,16.5,78 }          , 16.5   },{ {},{ .999,.999,.999 }, MaterialType::Refractive } },//Glas 
		SphereObject{ Sphere{ { 50,40,65 }            , 12.5 },{ {},{ .25,.75,.25 }   , MaterialType::Specular } },//---
		SphereObject{ Sphere{ { 70,68,65 }            , 12.5 },{ {},{ .25,.75,.75 }   , MaterialType::Diffuse } },//---
		SphereObject{ Sphere{ { 50,681.33,81.6 }      , 600    },{ { 12,12,12 },{}      , MaterialType::Diffuse } },//Lite 
	};

	Scene scene{ spheres, sizeof(spheres) / sizeof(spheres[0]) };
	Ray camera{ {50,52,295.6}, AntiVector{0, -0.042612,-1} };

	for (int i = 0; i < scene.sphereCount - 1; ++i)
		spheres[i].primaryLightSource = &spheres[scene.sphereCount - 1];

#else
	SphereObject spheres[] = {//Scene:                   , emission,        color,      material 
		SphereObject{ Sphere{ { 0,0,0 }  , 5 },{ {},{ .75,.25,.25 }   , MaterialType::DIFF } },//Left 
		SphereObject{ Sphere{ { 10,0,0 }  , 5 },{ {},{ .25,.25,.75 }   , MaterialType::DIFF } },//Left 
	};

	Scene scene{ spheres, sizeof(spheres) / sizeof(spheres[0]) };
	Ray camera{ { 0, 50, 40 }, Vector{ 0, -50, -40 }.normalized() };
#endif

	AntiVector cx = { width*.5135 / height,0,0 };
	AntiVector cy = AntiwedgeProduct(cx, camera.dir).leftComplement().normalized() * 0.5135;

	std::vector<uint8_t> data;
	data.resize(width * height * 3);
	
	std::mt19937 rnd;

	int numSamples = 16;

	for (int y = 0; y < height; ++y)
		for (int x = 0; x < width; ++x)
		{

			Color color = {};
			
			for (int s = 0; s < numSamples; ++s)
			{
				double dx = zeroToOne(rnd);
				double dy = zeroToOne(rnd);

				AntiVector d = camera.dir + cx * ((dx + x - width / 2) / width) - cy * ((dy + y - height / 2) / height);

				d = d.normalized();

				Color radiance = Radiance(scene, { camera.origin + d.rightComplement() * 140, d }, 0, rnd);
				color = color + radiance;
			}

			color = color * (1.0 / numSamples);

			double intensity = color.r > color.g ? (color.r > color.b ? color.r : color.b) : (color.b > color.g ? color.b : color.g);
			double knee = 0.75;
			double finalIntensity = intensity < knee ? intensity : knee + (1 - knee) * (intensity - knee) / (intensity - 2 * knee + 1);
			color = color * (finalIntensity / intensity);

			data[y * width * 3 + x * 3 + 0] = (uint8_t)(pow(color.r, 1/2.2) * 255);
			data[y * width * 3 + x * 3 + 1] = (uint8_t)(pow(color.g, 1/2.2) * 255);
			data[y * width * 3 + x * 3 + 2] = (uint8_t)(pow(color.b, 1/2.2) * 255);

			/*IntersectionPoint intersectionPoint;
			if (intersect(intersectionPoint, scene, { camera.origin + d.rightComplement() * 140, d }))
			{
				data[y * width * 3 + x * 3 + 0] = (uint8_t)(intersectionPoint.material.color.r * 255);
				data[y * width * 3 + x * 3 + 1] = (uint8_t)(intersectionPoint.material.color.g * 255);
				data[y * width * 3 + x * 3 + 2] = (uint8_t)(intersectionPoint.material.color.b * 255);
			}*/

		}

	stbi_write_png("res.png", width, height, 3, &data[0], width * 3);

    return 0;
}

